import org.testng.annotations.Test;
import org.testng.Assert;

public class PrintClassTest {
	@Test
	public void printMethodTest() {
	String str = "TestNG is working fine";
	Assert.assertEquals("TestNG is working fine", str);
	}
	
	@Test
	public void printMethodTest2() {
	String str = "TestNG is working fine 2";
	Assert.assertEquals("TestNG is working fine 2", str);
	}
	
	
}
