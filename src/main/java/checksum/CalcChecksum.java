/**
 * 
 */
package checksum;

/**
 * @author Cem Unuvar
 *
 */
public class CalcChecksum {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		// provide MotherLotId and separator as input
	    String MotherLotId = "6Q349007";
	    String Separator = ".";
	        
	    for (int i=1;i<=25;i++){
	    String j = String.format("%1$" + 2 + "s", String.valueOf(i)).replace(' ', '0');
	    System.out.println("Alias"+j+" "+MotherLotId+Separator+j+checkSumSemiM12(MotherLotId+Separator+j));
	    }
	}
	
	/**
	    * Calculate SEMI M12 checksum.
	    * @param token token for which to calculate the checksum
	    * @return checksum
	    */
	public static String checkSumSemiM12(final String token) {
		String checksum = "A0";

		final char[] tokenChar = token.toCharArray();
		double res = 0;
		double pow = tokenChar.length + 1;
		for (final char ch : tokenChar) {
			res += Math.pow(8, pow) * (ch - 32);
			pow--;
		}
		res += (8 * 33) + 16;
		res -= ((long) res / 59) * 59;
		if (res != 0) {
		final int val = (int) res;
		checksum = String.format("%c%c", (char) ((33 + (((59 - val) & 0x38) >> 3)) + 32), (char) ((16 + ((59 - val) & 0x7)) + 32));
   }
   return checksum;
}

}
