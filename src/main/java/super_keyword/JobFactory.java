package super_keyword;

public class JobFactory {
	
	public static Job createJob(String configName) throws Exception
	{
		Job result;
		
		if( configName.equals("TEST") )
		{
			result = new TestSetup(configName);
		}
		
		else
		{
			throw new Exception(String.format("Unknown job \"%s\"",configName));
		}
		
		return result;
	}
}