package super_keyword;

public class Job {

	private String jobName = "";

	public Job(String configName) {
		this.setJobName(configName);
	}

	public String getJobName() {
		return jobName;
	}

	public void setJobName(String jobName) {
		this.jobName = jobName;
	}

}
