package super_keyword;

class Student extends Person 
{ 
    Student() 
    { 
        // invoke or call parent class constructor 
        super(); 
  
        System.out.println("Student class Constructor"); 
    } 
}