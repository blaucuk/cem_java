package arraysum;

import java.io.*;
import java.util.*;

public class ArraySum {

    /*
     * Complete the simpleArraySum function below.
     */
    static int simpleArraySum(int[] ar) {
    	int sum = 0;
        for (int i = 0; i < ar.length; i++) {
        sum = sum + ar[i];
        }
        return sum;
    }

    private static final Scanner scanner = new Scanner(System.in);

    public static void main(String[] args) throws IOException {
        
        int arCount = Integer.parseInt(scanner.nextLine().trim());
        System.out.println(arCount);
        
        int[] ar = new int[arCount];
        //Print an Array using standard library Arrays
        System.out.println(Arrays.toString(ar));
        
        String[] arItems = scanner.nextLine().split(" ");
        System.out.println(Arrays.toString(arItems)+" is a String array");
        
        for (int arItr = 0; arItr < arCount; arItr++) {
            int arItem = Integer.parseInt(arItems[arItr].trim());
            ar[arItr] = arItem;
        }
        
        System.out.println(Arrays.toString(ar)+" is an int array");

        System.out.println(arCount);
        //Print an Array using standard library Arrays
        System.out.println(Arrays.toString(ar));
        System.out.println(Arrays.toString(arItems));
        
        int result = simpleArraySum(ar);
        System.out.println(result);
    }
}
