package file_operations;

import java.io.File;
import java.io.IOException;
import java.net.URISyntaxException;
import java.nio.file.Path;
import java.nio.file.Paths;

public class FileOperationsCem {

	public static void main(String[] args) throws URISyntaxException, IOException {
		FileOperationsCem foc = new FileOperationsCem();
		foc.readFileCem();
	}
	
	public void readFileCem() throws URISyntaxException, IOException {
		// Constructing a path object or resolving a child, does not mean the file or directory actually exists. 
		// The path is merely a reference to a potential file. So, youll have to separately verify its existence.
		Path path = Paths.get("/usr/local/home/cem/eclipse-workspace/Cem_TEST/bin/resources/sampleXML.xml");
		System.out.println(path);
		
		//Read a file:
		File file = new File(this.getClass().getResource("/resources/sampleXMLFile.xml").toURI());
		System.out.println(file);//getName());
		System.out.println(file.getPath() +"\n"+ file.getAbsolutePath() +"\n"+ file.getParent() +"\n"+ file.getCanonicalPath() +"\n"+ path.getFileName().toString());
	}

}
