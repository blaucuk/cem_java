package replacing;

public class ReplaceAllChars {

public static void main(String[] args) {

	        String stringUnderscoresForward = "j_u_s_t_a_s/t/r/i/n/g";
	        String stringWithDigits = "abcd12345efgh";
	        String stringWithWhiteSpaces = "s t r i n g";
	        String stringWithLowerCase = "This is a Lower Case String";

	        String finalString1 = stringUnderscoresForward.replaceAll("[_/]", "-");
	        String finalString2 = stringWithDigits.replaceAll("[\\d]", "");
	        String finalString3 = stringWithWhiteSpaces.replaceAll("[ ]", "");
	        String finalString4 = stringWithWhiteSpaces.replaceAll("[\\s]", "-");
	        String finalString5 = stringWithLowerCase.replaceAll("[\\p{Lower}]", "");

	        System.out.println("Old String: "+stringUnderscoresForward+" New String: "+finalString1);
	        System.out.println("Old String: "+stringWithDigits+" New String: "+finalString2);
	        System.out.println("Old String: "+stringWithWhiteSpaces+" New String: "+finalString3);
	        System.out.println("Old String: "+stringWithWhiteSpaces+" New String: "+finalString4);
	        System.out.println("Old String: "+stringWithLowerCase+" New String: "+finalString5);

	}	
}
