package com.blaucuk.www.filetest;

import java.io.File;
import java.io.FilenameFilter;
import java.util.Arrays;

public class FileTest {
	
	protected static final File InputDir = new File(File.separator+"home"+File.separator+"cuenueva"+File.separator+"tmp"+File.separator+"daily_tests"+File.separator+"tests_20200406"); //input directory
	
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		FileTest fileTest = new FileTest();
		fileTest.readFiles();
	}
	
	public void readFiles() {
		
    	//filter .txt files from TEST_INPUT
    	File[] files = InputDir.listFiles(
    			new FilenameFilter()
    	        {
    	          public boolean accept(File dir, String name)
    	          {
    	            return name.endsWith(".txt");
    	          }
    	        });
    	
    	//convert File array to String array
    	String[] FileNames = new String[files.length];
    	for (int i=0; i < files.length; i++) {
    		FileNames[i] = files[i].getName();
    	}
    	
    	System.out.println(Arrays.toString(FileNames));
    	
	}

}
