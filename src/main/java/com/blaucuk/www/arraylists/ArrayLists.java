/**
 * 
 */
package com.blaucuk.www.arraylists;

import java.util.*;

/**
 * @author cuenueva - test
 *
 */
public class ArrayLists {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		//The new item on the right should meet the promises of item on the left.
		ArrayList<Number> p1 = new ArrayList<Number>();
		//That array list of Numbers can not meet all the promises of ArrayList<Integer> 
		//a (i.e. an array list of Numbers may produce objects that are not Integers, even though its empty right then).
		ArrayList<Integer> p2 = new ArrayList<Integer>();
		//ArrayList<Integer> f1 = new ArrayList<Number>(); //won't compile
		//ArrayList<Number> f2 = new ArrayList<Integer>(); //won't compile
		List<? super Integer> p3=new ArrayList<Integer>();
		p3.add(1);
		p3.add(2); 
		Iterator<?> itr=p3.iterator(); 
		while(itr.hasNext()){ 
		System.out.println(itr.next());
		
		//--
		ArrayList<ArrayList<?>> p4 = new ArrayList<ArrayList<?>>();
		//ArrayList<?> f3 = new ArrayList<?>(); //won't compile. It isn't legal 
		//because when you're creating an instance of a parameterized class like ArrayList, 
		//you have to give a specific type parameter
		//--
		
		//--
		//PECS: Producer extends Consumer super
		List<? super Integer> p5=new ArrayList<Integer>(); //means Integer can be added to ?, iow, ? consumes Integer.
			
		//--
		}

	}

}
